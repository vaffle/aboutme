package buu.withiphong.aboutme

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.withiphong.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myName:MyName = MyName("Withiphong Loetphaisanworakun")
    lateinit var myButton: Button
    lateinit var editText: EditText
    lateinit var nicknameTextView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.apply {
            myButton = doneButton
            editText = nicknameEdit
            nicknameTextView = nicknameText
            doneButton.setOnClickListener {v->
                addNickname(v)
            }
            nicknameText.setOnClickListener{ it->
                updateNickname(it)
            }
            this.myName = this@MainActivity.myName
        }


    }
    fun addNickname(v: View){
        binding.apply {
            myButton.visibility = View.GONE
            editText.visibility = View.GONE
            myName?.nickname = editText.text.toString()
            nicknameTextView.visibility = View.VISIBLE
            invalidateAll()
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    private fun updateNickname(v: View){
        editText.visibility = View.VISIBLE
        myButton.visibility = View.VISIBLE
        v.visibility = View.GONE
        editText.requestFocus()
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(editText, 0)

    }
}